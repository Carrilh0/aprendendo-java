package primeiroprograma;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class PrimeiroPrograma {

    public static void main (String[] args){ 
        
        Date data = new Date(); 
        SimpleDateFormat formato = new SimpleDateFormat("dd/M/yyyy");
        String strDate = formato.format(data);
        System.out.println(strDate);
        
        /////////////////////////////////////////////////////////////////
        
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension tela = toolkit.getScreenSize();
        
        int width = (int)tela.getWidth();
        int heigth = (int)tela.getHeight();
        
        System.out.println("A resolução atual é "+width+"x"+heigth);
        
        ///////////////////////////////////////////////////////////////////
        
        Locale loc = Locale.getDefault();
        String idioma = loc.getDisplayLanguage();
        System.out.println("O idioma atual é: "+idioma);
    }
}
